package exebuilder.ui;

import exebuilder.core.Windows;
import javafx.scene.control.Alert;

public class MessageBox {

  public static void error(String text) {
    var alert = new Alert(Alert.AlertType.ERROR);
    alert.setTitle("错误");
    alert.setHeaderText(null);
    alert.setContentText(text);
    alert.initOwner(Windows.get("main"));
    alert.showAndWait();
  }

}
